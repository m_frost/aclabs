.include "nios_macros.s"


.text
.equ TEXT_NUM, 0x3b
.global _start
 
_start:

	movia r7, TEXT_NUM
	mov r4, r7
STRING_COUNTER:
	mov r2, r0
STRING_COUNTER_LOOP:
	beq r4, r0, END_STRING_COUNTER
	srli r5, r4, 1
	and r4, r4, r5
	addi r2, r2, 1
	br STRING_COUNTER_LOOP
END_STRING_COUNTER:
	mov r16, r2
END:
	br END
.end